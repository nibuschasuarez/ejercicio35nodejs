//multiplicar
const fs = require('fs');

const crearArchivo = (base = 1) => {
    try {

  let salida = '';

  console.log('=====================');
  console.log(`    Tabla del ${base}       `);
  console.log('=====================');

  for(let i = 1; i <= 100; i++){
    salida += `${base} x ${i} = ${base * i}\n`;
  }

  console.log(salida);

    fs.writeFileSync(`tabla-${base}.txt`, salida);
   
  }
  catch (error) {
    throw error;
  }
};
//Exportamos el archivo

//Suma  




const crearArchivo1 = (base= 1) => {
  try {
    let salida = '';

  console.log('=====================');
  console.log(`    Tabla del ${base}      `);
  console.log('=====================');
  for(let i = 1; i <= 100; i++){
    salida += `${base} + ${i} = ${base + i}\n`;
  }

  console.log(salida);

    fs.writeFileSync(`tabla-${base}.txt`, salida);
  }
  catch (error) {
    throw error;
    
  }
  };  

  //Resta
  
  


const crearArchivo2 = (base= 1) => {
  try {
    let salida = '';

  console.log('=====================');
  console.log(`    Tabla del ${base}      `);
  console.log('=====================');
  for(let i = 1; i <= 5; i++){
    salida += `${base} - ${i} = ${base - i}\n`;
  }

  console.log(salida);

    fs.writeFileSync(`tabla-${base}.txt`, salida);
  }
  catch (error) {
    throw error;
    
  }
  }; 
   
  //Dividir 


  const crearArchivo3 = (base= 1) => {
    try {
      let salida = '';
  
    console.log('=====================');
    console.log(`    Tabla del ${base}      `);
    console.log('=====================');
    for(let i = 1; i <= 5; i++){
      salida += `${base}  ÷  ${i} = ${base / i}\n`;
    }
  
    console.log(salida);
  
      fs.writeFileSync(`tabla-${base}.txt`, salida);
    }
    catch (error) {
      throw error;
      
    }
    };


  module.exports = {
    generarArchivo: crearArchivo,
    generarArchivo1: crearArchivo1,
    generarArchivo2: crearArchivo2,
    generarArchivo3: crearArchivo3
  };
